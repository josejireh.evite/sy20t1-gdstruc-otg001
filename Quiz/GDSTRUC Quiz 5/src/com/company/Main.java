package com.company;

public class Main {

    public static void main(String[] args) {
        // Instantiate an Array
        int[] randomNumbers = new int[10];

        randomNumbers[0] = 12;
        randomNumbers[1] = 23;
        randomNumbers[2] = 66;
        randomNumbers[3] = 2;
        randomNumbers[4] = 98;
        randomNumbers[5] = 97;
        randomNumbers[6] = 56;
        randomNumbers[7] = 48;
        randomNumbers[8] = -67;
        randomNumbers[9] = 2;

        bubbleSort(randomNumbers);
        printArrayElements(randomNumbers);

        System.out.println(" ");
        System.out.println("======================================================");
        System.out.println(tripleGroupSort(randomNumbers,69));

        System.out.println("======================================================");
        System.out.println(tripleGroupSort(randomNumbers,97));
    }

    // Sort it in ascending order from  Quiz 1
    private static void bubbleSort(int[] array)
    {
        for(int lastSortedIndex = array.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for(int i = 0; i < lastSortedIndex; i++)
            {
                if(array[i] > array[i+1])
                {
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                }
            }
        }
    }

    // Print Function
    private static void printArrayElements(int[] array)
    {
        for (int j : array)
        {
            System.out.print(j + " ");
        }
    }

    // Evite Triple Group Search Algorithm
    private static int tripleGroupSort(int[] array, int value)
    {
        int arrayTrueLength = array.length - 1;
        double oneThirdDouble = arrayTrueLength * 0.33;
        int oneThird = (int)oneThirdDouble;
        double twoThirdDouble = arrayTrueLength * 0.66;
        int twoThird = (int)twoThirdDouble;


        if(value <= array[arrayTrueLength])
        {
            if(value <= array[twoThird])
            {
                if(value <= array[oneThird])
                {
                    for(int i = oneThird; i > 0; i--)
                    {
                        if(array[i] == value)
                        {
                            return i;
                        }
                    }
                }
                else
                    {
                        for(int i = twoThird; i > oneThird; i--)
                        {
                            if(array[i] == value)
                            {
                                return i;
                            }
                        }
                    }
            }
            else
                {
                    for(int i = arrayTrueLength; i > twoThird; i--)
                    {
                        if(array[i] == value)
                        {
                            return i;
                        }
                    }
                }
        }
        return -1;
    }
}
