package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] numbers = new int[10];

        numbers[0] = 12;
        numbers[1] = 65;
        numbers[2] = 7;
        numbers[3] = 54;
        numbers[4] = 27;
        numbers[5] = 76;
        numbers[6] = 32;
        numbers[7] = 48;
        numbers[8] = -25;
        numbers[9] = 57;

        System.out.println("Random Numbers");
        printArrayElements(numbers);

        System.out.println("\n\nBubble Sort in descending order");
        bubbleSort(numbers);
        printArrayElements(numbers);

        System.out.println("\n\nSelection Sort in descending order");
        selectionSort(numbers);
        printArrayElements(numbers);

        System.out.println("\n\nModified Selection Sort");
        modifiedSelectionSort(numbers);
        printArrayElements(numbers);

    }

    private static void selectionSort(int[] array)
    {
        for (int firstSortedIndex = 0; firstSortedIndex > 0; firstSortedIndex++)
        {
            int smallestIndex = array.length - 1;

            for (int i = 1; i <= firstSortedIndex; i++)
            {
                if(array[i] > array[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
            int temp = array[firstSortedIndex];
            array[firstSortedIndex] = array[smallestIndex];
            array[smallestIndex] = temp;
        }
    }

    private static void modifiedSelectionSort(int[] array)
    {
        for (int lastSortedIndex = array.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int largestIndex = 0;

            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if(array[i] < array[largestIndex])
                {
                    largestIndex = i;
                }
            }
            int temp = array[lastSortedIndex];
            array[lastSortedIndex] = array[largestIndex];
            array[largestIndex] = temp;
        }
    }

    private static void bubbleSort(int[] array)
    {
        for(int lastSortedIndex = array.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for(int i = 0; i < lastSortedIndex; i++)
            {
                if(array[i] < array[i + 1])
                {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
    }

    private static void printArrayElements(int[] array)
    {
        for (int j : array) {
            System.out.print(j + " ");
        }
    }
}
