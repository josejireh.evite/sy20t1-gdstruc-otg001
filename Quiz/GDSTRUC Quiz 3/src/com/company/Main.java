package com.company;

import java.util.Random;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        // Instantiating
        ArrayQueue inQueue = new ArrayQueue(14);
        ArrayQueue inGame = new ArrayQueue(50);
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        int turnCount = 1;
        int gameCount = 0;

        while(gameCount != 10)
        {
            // HUD
            System.out.println("======================");
            System.out.println("Turn " + turnCount);
            System.out.println("Game/s made " + gameCount);

            // RNG For how many player will enter the queue +1 to offset the 0
            int playerCountRNG = random.nextInt(7);
            playerCountRNG += 1;

            // Player Creation Function
            playerCreation(playerCountRNG, scanner, inQueue);

            // Conditional if there are 5 player in queue a game will start
            if(inQueue.size() >= 5)
            {
                System.out.println("5/5 in queue, A game is starting!!!");

                // I stored the popped Players in an another queue as well
                for(int i = 0; i < 5; i++)
                {
                    inGame.add(inQueue.remove());
                }

                gameCount++;
            }

            // Displays Players that are left in queue
            System.out.println("--------------------");
            System.out.println("Players Left in Queue");
            inQueue.printQueue();

            // System Pause
            System.out.println("--------------------");
            System.out.println("Press Enter to End Turn");
            scanner.nextLine();

            // Increments
            turnCount++;
        }

        // End Game Display
        System.out.println("======================");
        System.out.println("Program Ended 10 games have been successfully made!!!");
        System.out.println("--------------------");
        System.out.println("Players Left in Queue");
        inQueue.printQueue();
        System.out.println("--------------------");
        System.out.println("Players in Game");
        inGame.printQueue();
    }

    // User Input for the names of the player wanting to queue up
    public static void playerCreation(int playerCountRNG, Scanner scanner, ArrayQueue queue)
    {
        System.out.println("Choose a name for the "+ playerCountRNG +" player/s entering the queue");
        for(int i = 0; i < playerCountRNG; i++)
        {
            String input = scanner.nextLine();
            queue.add(new Player(input));
        }
    }

}
