package com.company;

public class Hashtable {
    private StoredPlayer[] hashtable;

    // Constructor
    public Hashtable()
    {
        hashtable = new StoredPlayer[10];
    }

    // hashkey - ing
    private int hashKey(String key)
    {
        return key.length() % hashtable.length;
    }

    // Adding Elements to the hashtable w/ linear probing
    public void put(String key, Player value)
    {
        int hashedKey = hashKey(key);

        if(isOccupied(hashedKey))
        {
            // Linear Probing
            int stoppingIndex = hashedKey;

            if(hashedKey == hashtable.length - 1)
            {
                hashedKey = 0;
            }
            else
            {
                hashedKey++;
            }

            while (isOccupied(hashedKey) && hashedKey != stoppingIndex)
            {
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }

        if(isOccupied(hashedKey))
        {
            System.out.println("There is an element present already at position" + hashedKey);
        }
        else
        {
            hashtable[hashedKey] = new StoredPlayer(key, value);
        }
    }

    // Accessing Elements in the Hashtable
    public Player get(String key)
    {
        int hashedKey = findKey(key);

        if(hashedKey == -1)
        {
            return null;
        }

        return hashtable[hashedKey].value;
    }

    // Removing an Element in the hashtable
    public StoredPlayer remove(String key)
    {
        int hashedKey = findKey(key);

        if(hashedKey == -1)
        {
            return null;
        }

        StoredPlayer holder = hashtable[hashedKey];
        hashtable[hashedKey] = null;
        return holder;
    }

    // Checking if the index on the hashtable is occupied
    private boolean isOccupied(int index)
    {
        return hashtable[index] != null;
    }

    // Checking if the key is same with earlier linear probing
    private int findKey(String key)
    {
        int hashedKey = hashKey(key);

        // Finding the Right Key
        if (hashtable[hashedKey] != null
                && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }

        // Linear Probing again
        int stoppingIndex = hashedKey;

        if (hashedKey == hashtable.length - 1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }

        while (hashedKey != stoppingIndex
                && hashtable[hashedKey] != null
                && !hashtable[hashedKey].key.equals(key))
        {
            hashedKey = (hashedKey + 1) % hashtable.length;
        }

        if(hashtable[hashedKey] != null
                && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }
        return -1;
    }

    // Printing
    public void printHashTable()
    {
        for (int i = 0; i < hashtable.length; i++)
        {
            if (hashtable[i] != null)
            {
                System.out.println("Element " + i + " " + hashtable[i].value);
            }
            else
            {
                System.out.println("Element " + i + " is null");
            }
        }
    }

}
