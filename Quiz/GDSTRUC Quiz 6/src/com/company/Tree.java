package com.company;

public class Tree {
    private Node root;

    // Insert Function
    public void insert(int value)
    {
        if(root == null)
        {
            root = new Node(value);
        }
        else
        {
            root.insert(value);
        }
    }

    // Traverse the Tree in ascending Order
    public void traverseInOrder()
    {
        if(root != null)
        {
            root.traverseInOrder();
        }
    }

    // Traverse the Tree in descending Order
    public void traverseInOrderDescending()
    {
        if(root != null)
        {
            root.traverseInOrderDescending();
        }
    }

    // Get Function
    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

    // Get Node Minimum Function
    public Node getMin()
    {
        if (root != null)
        {
            return root.getMin();
        }
        return null;
    }

    // Get Node Maximum Function
    public Node getMax()
    {
        if (root != null)
        {
            return root.getMax();
        }
        return null;
    }
}
