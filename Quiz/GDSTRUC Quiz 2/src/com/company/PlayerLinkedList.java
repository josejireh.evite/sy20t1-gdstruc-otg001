package com.company;

import java.util.Objects;

public class PlayerLinkedList {
    private PlayerNode head;
    private int size = 0;

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;

        // Size Variable Checker
        PlayerNode current = head;
        int count = 0;
        while(current != null){
            count++;
            current = current.getNextPlayer();
        }
        size = count;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("Head -> ");
        while(current != null){
            System.out.print(current.getPlayer());
            System.out.print("-> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
        System.out.print("Size = ");
        System.out.println(size);
    }

    // Quiz Stuff
    public void deleteFirstElement()
    {
        PlayerNode newHeadNode = head;
        newHeadNode = newHeadNode.getNextPlayer();
        head.setNextPlayer(null);
        head = newHeadNode;

        // Size Variable Checker
        PlayerNode current = head;
        int count = 0;
        while(current != null){
            count++;
            current = current.getNextPlayer();
        }
        size = count;
    }

    public boolean contains(Player player){

        boolean torF = false;

        PlayerNode current = head;
        while(current != null){
            if(player == current.getPlayer())
            {
                torF = true;
            }
            current = current.getNextPlayer();
        }
        return torF;
    }

    public int indexOf(Player player)
    {
        int count = -1;

        PlayerNode current = head;
        while(current != null){
            count++;
            if(player == current.getPlayer())
            {
                continue;
            }
            current = current.getNextPlayer();
        }
        return count;
    }
}
