package com.company;

public class Main {

    public static void main(String[] args) {
	    // Initialization of Players
        Player doublelift = new Player(1, "TSM lift", 124);
        Player huhi = new Player(2, "Huhi", 14);
        Player xpecial = new Player(3, "TSM xpecial", 140);
        Player brax = new Player(4, "T1 brax", 124);
        Player ocelote = new Player(5, "G2 ocelote", 12);
        Player xpeke = new Player(6, "FNC xpeke", 4);
        Player scarra = new Player(7, "OTV scarra", 300);
        Player faker = new Player(8, "T1 faker", 999);
        Player s1mple = new Player(9, "NVI s1mple", 777);
        Player trick2g = new Player(10, "GATES trick", 69);

        Hashtable hashtable = new Hashtable();
        hashtable.put(doublelift.getUserName(), doublelift);
        hashtable.put(huhi.getUserName(), huhi);
        hashtable.put(xpecial.getUserName(), xpecial);
        hashtable.put(brax.getUserName(), brax);
        hashtable.put(ocelote.getUserName(), ocelote);
        hashtable.put(xpeke.getUserName(), xpeke);
        hashtable.put(scarra.getUserName(), scarra);
        hashtable.put(faker.getUserName(), faker);
        hashtable.put(s1mple.getUserName(), s1mple);
        hashtable.put(trick2g.getUserName(), trick2g);

        hashtable.printHashTable();

        System.out.println("======================================================");
        System.out.println("Getting trick2g");
        System.out.println(hashtable.get("GATES trick"));

        System.out.println("======================================================");
        System.out.println("Removing trick2g");
        hashtable.remove("GATES trick");
        hashtable.printHashTable();

        System.out.println("======================================================");
        System.out.println("Putting back trick2g");
        hashtable.put(trick2g.getUserName(), trick2g);
        hashtable.printHashTable();

    }
}
