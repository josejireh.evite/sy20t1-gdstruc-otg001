package com.company;

public class Main {

    public static void main(String[] args) {

        Tree tree = new Tree();

        tree.insert(12);
        tree.insert(2);
        tree.insert(87);
        tree.insert(55);
        tree.insert(78);
        tree.insert(9);
        tree.insert(35);
        tree.insert(100);
        tree.insert(34);
        tree.insert(92);

        //tree.traverseInOrder();
        //System.out.println(tree.get(100));

        System.out.println(tree.getMin());

        System.out.println(tree.getMax());

        tree.traverseInOrderDescending();
    }
}
