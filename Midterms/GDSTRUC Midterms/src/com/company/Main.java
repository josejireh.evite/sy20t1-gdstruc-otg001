package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        // Instantiate dem Stacks
        CardStack playerDeck = new CardStack(30);
        CardStack discardPile = new CardStack(30);
        CardStack hand = new CardStack(30);

        // Populate the Player Deck with cards
        populateDeck(playerDeck);

        // The Game
        int turnCount = 1;
        while(playerDeck.size() != 0)
        {
            // HUD
            System.out.println("Turn: " + turnCount);
            System.out.println("Player Deck: " + playerDeck.size());
            System.out.println("Discard Pile: " + discardPile.size());
            System.out.println("----------------");
            System.out.println("is Holding: ");
            hand.printStack();
            System.out.println("----------------");

            // RNG on whether what command to pick
            Random random = new Random();
            int commandRNG = random.nextInt(3);

            // If statement for what Command to do
            System.out.println("==================================");
            if(commandRNG == 0)
            {
                playerDraw(playerDeck, hand, random);
            }
            else if(commandRNG == 1)
            {
                playerDiscard(playerDeck, discardPile, random);
            }
            else if(commandRNG == 2)
            {
                playerReturnDiscarded(hand, discardPile, random);
            }
            System.out.println("==================================");

            // Increment the Turn Every Loop
            turnCount++;
        }

        // Game Evaluator
        System.out.println("Game Ended[Player Deck is Empty]");
    }

    // Functions
    private static void populateDeck(CardStack playerDeck)
    {
        playerDeck.push(new Card("King"));
        playerDeck.push(new Card("Queen"));
        playerDeck.push(new Card("Jack"));
        playerDeck.push(new Card("Ace"));
        playerDeck.push(new Card("Joker"));
        playerDeck.push(new Card("King"));
        playerDeck.push(new Card("Queen"));
        playerDeck.push(new Card("Jack"));
        playerDeck.push(new Card("Ace"));
        playerDeck.push(new Card("Joker"));
        playerDeck.push(new Card("King"));
        playerDeck.push(new Card("Queen"));
        playerDeck.push(new Card("Jack"));
        playerDeck.push(new Card("Ace"));
        playerDeck.push(new Card("Joker"));
        playerDeck.push(new Card("King"));
        playerDeck.push(new Card("Queen"));
        playerDeck.push(new Card("Jack"));
        playerDeck.push(new Card("Ace"));
        playerDeck.push(new Card("Joker"));
        playerDeck.push(new Card("King"));
        playerDeck.push(new Card("Queen"));
        playerDeck.push(new Card("Jack"));
        playerDeck.push(new Card("Ace"));
        playerDeck.push(new Card("Joker"));
        playerDeck.push(new Card("King"));
        playerDeck.push(new Card("Queen"));
        playerDeck.push(new Card("Jack"));
        playerDeck.push(new Card("Ace"));
        playerDeck.push(new Card("Joker"));

    }

    // All of the RNG has an increment of  1 to offset the 0 to make 0 - 4 into 1 - 5
    private static void playerDraw(CardStack playerDeck, CardStack hand, Random random)
    {
        int xRNG = random.nextInt(5);
        xRNG += 1;

        if(xRNG > playerDeck.size())
        {
            xRNG = playerDeck.size();
        }
        System.out.println("Player is drawing " + xRNG + " card/s !!!");

        for (int i = 0; i < xRNG; i++) {
            hand.push(playerDeck.pop());
        }
    }

    private static void playerDiscard(CardStack playerDeck, CardStack discardPile, Random random)
    {
        int xRNG = random.nextInt(5);
        xRNG += 1;

        if(xRNG > playerDeck.size())
        {
            xRNG = playerDeck.size();
        }
        System.out.println("Player is discarding " + xRNG + " card/s !!!");

        for (int i = 0; i < xRNG; i++) {
            discardPile.push(playerDeck.pop());
        }
    }

    private static void playerReturnDiscarded(CardStack hand, CardStack discardPile, Random random)
    {
        int xRNG = random.nextInt(5);
        xRNG += 1;

        if(xRNG > discardPile.size())
        {
            xRNG = discardPile.size();
        }
        System.out.println("Player is returning " + xRNG + " card/s from the discard pile to hand!!!");

        for (int i = 0; i < xRNG; i++)
        {
            hand.push(discardPile.pop());
        }
    }
}
